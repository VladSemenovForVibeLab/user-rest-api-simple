# Учет пользователей

Проект реализует учет пользователей с использованием базы данных MariaDB и фреймворка Spring Boot. Приложение предоставляет CRUD операции для управления именами и телефонами пользователей.

## Технологии

- Java 17
- Spring Boot 2.7.17
- Spring Data JPA
- Spring Boot Starter Web
- Lombok
- Maven

## Установка и запуск

1. Установите и настройте базу данных MariaDB.
2. Создайте новую базу данных для приложения.
3. Установите Java 17 и Maven.
4. Клонируйте репозиторий на свой компьютер.
5. Откройте проект в вашей среде разработки.
6. Настройте параметры подключения к базе данных в файле `application.properties`.
7. Запустите приложение.

## Описание API

### Создание пользователя

**Endpoint**: `POST /user/create`

**Пример запроса**:
```json
{
  "name":"{{$randomFirstName}}",
  "phone":"{{$randomPhoneNumber}}"
}
```

**Пример успешного ответа**:
```json
{
  "id": 11,
  "uid": "Coding--360101018-Moments",
  "name": "Agnes",
  "phone": "585-454-0815"
}
```

### Получение пользователя по идентификатору

**Endpoint**: `GET /user/{id}`

**Пример успешного ответа**:
```json
{
  "id": 3,
  "uid": "Coding--239635921-Moments",
  "name": "Lennie",
  "phone": "947-790-1399"
}
```

### Обновление пользователя

**Endpoint**: `PUT /user/{id}`

**Пример запроса**:
```json
{
  "name": "Francezfasfassca",
  "phone": "886-257-7530"
}
```

**Пример успешного ответа**:
```json
{
  "id": 2,
  "uid": "Coding-755297366-Moments",
  "name": "Francezfasfassca",
  "phone": "886-257-7530"
}
```

### Удаление пользователя

**Endpoint**: `DELETE /user/{id}`

**Пример успешного ответа**: Record is deleted having id: 2

### Получение списка всех пользователей

**Endpoint**: `GET /users`

**Пример успешного ответа**:
```json
[
  {
    "id": 3,
    "uid": "Coding--239635921-Moments",
    "name": "Lennie",
    "phone": "947-790-1399"
  },
  {
    "id": 4,
    "uid": "Coding-1362860842-Moments",
    "name": "Kraig",
    "phone": "895-978-5696"
  },
  {
    "id": 5,
    "uid": "Coding--1546384569-Moments",
    "name": "Mateo",
    "phone": "359-540-9127"
  },
  {
    "id": 6,
    "uid": "Coding--291066608-Moments",
    "name": "Hunter",
    "phone": "499-414-9435"
  },
  {
    "id": 7,
    "uid": "Coding-786019959-Moments",
    "name": "Oscar",
    "phone": "886-692-8975"
  },
  {
    "id": 8,
    "uid": "Coding--1118428837-Moments",
    "name": "Davonte",
    "phone": "640-533-3387"
  },
  {
    "id": 9,
    "uid": "Coding-1310773102-Moments",
    "name": "Millie",
    "phone": "803-862-0342"
  },
  {
    "id": 10,
    "uid": "Coding--572276481-Moments",
    "name": "Yoshiko",
    "phone": "406-739-6474"
  },
  {
    "id": 11,
    "uid": "Coding--360101018-Moments",
    "name": "Agnes",
    "phone": "585-454-0815"
  }
]
```