package com.cm.userApp.userRestApi.controller;

import com.cm.userApp.userRestApi.model.user.UserEntity;
import com.cm.userApp.userRestApi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public ResponseEntity<UserEntity> createUser(@RequestBody UserEntity userEntity){
        return new ResponseEntity<>(userService.createUser(userEntity),
                HttpStatus.CREATED);
    }
    @GetMapping("/{id}")
    public Optional<UserEntity> searchById(@PathVariable Integer id){
        return userService.searchById(id);
    }
    @GetMapping("/")
    public List<UserEntity> getAllUser(){
        return userService.getAllUser();
    }
    @PutMapping("/{id}")
    public ResponseEntity<UserEntity> updateUserById(@PathVariable Integer id,
                                                     @RequestBody UserEntity userEntity){
        return new ResponseEntity<>(userService.updateById(id,userEntity),HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable Integer id){
        userService.deleteById(id);
        return "Record is deleted having id: "+id;
    }
}
