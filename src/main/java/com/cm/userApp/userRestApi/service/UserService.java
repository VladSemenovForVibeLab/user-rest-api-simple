package com.cm.userApp.userRestApi.service;

import com.cm.userApp.userRestApi.model.user.UserEntity;
import com.cm.userApp.userRestApi.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class UserService {
    @Autowired
    private UserRepo userRepo;
    public UserEntity createUser(UserEntity userEntity) {
        Random random = new Random();
        int id = random.nextInt();
        String id1 = "Coding-"+id+"-Moments";
        userEntity.setUid(id1);
        return userRepo.save(userEntity);
    }

    public Optional<UserEntity> searchById(Integer id) {
        return userRepo.findById(id);
    }

    public List<UserEntity> getAllUser() {
        return userRepo.findAll();
    }

    public UserEntity updateById(Integer id, UserEntity userEntity) {
        UserEntity searchedEntity = userRepo.getById(id);
        searchedEntity.setName(userEntity.getName());
        searchedEntity.setPhone(userEntity.getPhone());
        return userRepo.save(searchedEntity);
    }

    public void deleteById(Integer id) {
        userRepo.deleteById(id);
    }
}
